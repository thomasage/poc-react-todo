import React, { useState } from 'react'
import PropTypes from 'prop-types'

function TaskAdd ({ onSubmit }) {
  const [title, setTitle] = useState('')

  function handleSubmit (event) {
    event.preventDefault()
    onSubmit(title)
    setTitle('')
  }

  return (
        <form onSubmit={event => handleSubmit(event)} className="mt-4">
            <div className="d-flex flex-row align-items-center">
                <label htmlFor="title" className="text-nowrap">New task</label>
                <input type="text"
                       className="form-control ms-3 me-3"
                       name="title"
                       value={title}
                       onChange={(event => setTitle(event.target.value))}
                       id="title"
                       autoFocus/>
                <button type="submit" className="btn btn-success">Add</button>
            </div>
        </form>
  )
}

TaskAdd.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

export default TaskAdd
