import React from 'react'
import PropTypes from 'prop-types'
import TaskItem from './TaskItem'

function TaskList ({ onDelete, tasks }) {
  return (
        <ul className="list-group mt-4">
            {tasks.map(task => (
                <TaskItem key={task.uuid}
                          onDelete={uuid => onDelete(uuid)}
                          title={task.title}
                          uuid={task.uuid}/>
            ))}
        </ul>
  )
}

TaskList.propTypes = {
  onDelete: PropTypes.func.isRequired,
  tasks: PropTypes.array.isRequired
}

export default TaskList
