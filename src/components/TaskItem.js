import React from 'react'
import PropTypes from 'prop-types'

function TaskItem ({ onDelete, title, uuid }) {
  return (
            <li className="list-group-item p-3 d-flex flex-row align-items-center">
                <div className="flex-grow-1">
                    {title}<br/>
                    <small className="text-muted">{uuid}</small>
                </div>
                <button type="button"
                        className="btn btn-outline-danger btn-sm"
                        onClick={() => onDelete(uuid)}>
                    Remove
                </button>
            </li>
  )
}

TaskItem.propTypes = {
  onDelete: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  uuid: PropTypes.string.isRequired
}

export default TaskItem
