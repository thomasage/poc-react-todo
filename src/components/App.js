import React, { useState } from 'react'
import TaskAdd from './TaskAdd'
import TaskList from './TaskList'
import { v4 as uuidv4 } from 'uuid'

function App () {
  const savedTasks = localStorage.getItem('tasks')
  const [tasks, updateTasks] = useState(savedTasks ? JSON.parse(savedTasks) : [])

  function onAdd (title) {
    const task = {
      title,
      uuid: uuidv4()
    }
    const newTasks = [...tasks, task]
    newTasks.sort((a, b) => a.title.localeCompare(b.title))
    updateTasks(newTasks)
    saveTasks(newTasks)
  }

  function onDelete (uuid) {
    const newTasks = tasks.filter(task => task.uuid !== uuid)
    updateTasks(newTasks)
    saveTasks(newTasks)
  }

  function saveTasks (tasks) {
    localStorage.setItem('tasks', JSON.stringify(tasks))
  }

  return (
        <div>
            <TaskAdd tasks={tasks} onSubmit={title => onAdd(title)}/>
            <TaskList tasks={tasks} onDelete={uuid => onDelete(uuid)}/>
        </div>
  )
}

export default App
